var JSX = require('node-jsx').install(),
    React = require('react'),
    request = require('request'),
    ToDoApp = React.createFactory(require('./components/ToDoApp.react'));

module.exports = {

  index: function(req, res) {
    request("http://127.0.0.1:5000/api/todos", function (err, result) {
      var data = JSON.parse(result.body);

      var markup = React.renderToString(
        ToDoApp({
          todos: data.todos
        })
      );

      res.render("todos", {
        markup: markup,
        state: JSON.stringify(data.todos)
      });
    });
  },

  add: function(req, res) {
    var options = {
      url: "http://127.0.0.1:5000/api/todos",
      body: req.body,
      json: true,
      method: "post"
    };
    request(options).pipe(res);
    if (req.headers['content-type'] === 'application/x-www-form-urlencoded') {
        res.redirect('/');
    }
  },

  update: function (req, res) {
    var options = {
      url: "http://127.0.0.1:5000/api/todos/" + req.params.id,
      method: "put",
      json: true,
      body: req.body
    };
    req.pipe(request(options)).pipe(res);
  } 
};
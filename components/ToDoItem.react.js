var React = require('react');

var ToDoItem = React.createClass({
	toggleDone: function () {
		var isDone = this.refs.isDoneCheckbox.getDOMNode().checked;
		var updatedItem = this.props.item;
		this.props.onToggleDone(this.props.item, isDone);
	},
	render: function () {
		var className = (this.props.item.done ? "done" : "");
		return (
			<li className={className}>
			<input type="checkbox" checked={this.props.item.done} onChange={this.toggleDone} ref="isDoneCheckbox" />
			{this.props.item.text}
			</li>);
	}
});
module.exports = ToDoItem;
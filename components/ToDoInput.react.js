var React = require('react');

var ToDoInput = React.createClass({
	addToDo: function (event) {
		event.preventDefault();
		var text = this.refs.text.getDOMNode().value.trim();
		if (text.length) {
			this.props.onToDoSubmit({ text : text});
			this.refs.text.getDOMNode().value = '';
		}
	},
	render: function () {
		return (
		<form onSubmit={this.addToDo} method="POST" action="/todos/add">
			<input type="text" name="text" placeholder="What needs to be done?" ref="text" />
			<button>Add Todo</button>
		</form>);
	}
});
module.exports = ToDoInput;
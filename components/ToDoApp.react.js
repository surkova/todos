var React = require('react');
var ToDoHeader = require('./ToDoHeader.react');
var ToDoInput = require('./ToDoInput.react');
var ToDoList = require('./ToDoList.react');
var ToDoFooter = require('./ToDoFooter.react');

var ToDoApp = React.createClass({
	getInitialState: function () {
		return { 
			todos: this.props.todos,
			undone: this.props.todos.filter(function (todo) {
						return !todo.done;
					}).length
		};
	},
	handleToDoSubmit: function (todo) {
		var todos = this.state.todos;
		var updatedTodos = todos.concat([{
			text: todo.text,
			id: todos[todos.length - 1]['id'] + 1,
			done: false
		}]);
		this.setState({ 
			todos : updatedTodos,
			undone: ++this.state.undone
		});
		this.postToDoItem(todo);
	},
	postToDoItem: function (todo) {
		$.ajax({
			url: this.props.addUrl,
			dataType: 'json',
			type: 'POST',
			data: todo,
			success: function(data) {
				this.setState({
					todos: data.todos,
					undone: data.todos.filter(function (todo) {
						return !todo.done;
					}).length
				});
			}.bind(this),
			error: function(xhr, status, err) {
				console.error(this.props.addUrl, status, err.toString());
			}.bind(this)
    	});
	},
	putToDoItem: function (todo, isDone) {
		$.ajax({
			url: this.props.updateUrl + todo.id,
			method: 'put',
			data: todo,
			success: function (data) {
				this.setState({
					todos: data.todos,
					undone: data.todos.filter(function (todo) {
						return !todo.done;
					}).length
				});
			}.bind(this),
			error: function(xhr, status, err) {
	        	console.error(this.props.updateUrl, status, err.toString());
	      	}.bind(this)
		});
	},
	handleToDoToggle: function (todo, isDone) {
		var updatedToDo = todo;
		updatedToDo.done = isDone;
		var todos = this.state.todos;
		var index = todos.indexOf(todo);
		todos.splice(index, 1, updatedToDo);
		this.setState({
			todos: todos,
			undone: (isDone ? --this.state.undone : ++this.state.undone)
		});
		this.putToDoItem(updatedToDo, isDone);
	},
	handleMarkAll: function () {
		var todos = this.state.todos.map(function (todo) {
			if (!todo['done']) {
				todo['done'] = true;
				$.ajax({
					url: this.props.updateUrl + todo.id,
					method: 'put',
					data: todo
				});
			} 
			return todo;
		}, this);

		this.setState({ 
			todos: todos,
			undone: 0
		});
	},
	render: function () {
		return (
			<div>
				<ToDoHeader />
				<ToDoInput onToDoSubmit={this.handleToDoSubmit} />
				<ToDoList items={this.state.todos} onToDoToggle={this.handleToDoToggle} />
				<ToDoFooter undone={this.state.undone} onMarkAll={this.handleMarkAll} />
			</div>
		);
	}
});
module.exports = ToDoApp;
var React = require('react');
var ToDoItem = require('./ToDoItem.react');

var ToDoList = React.createClass({
	handleToggle: function (todo, isDone) {
		this.props.onToDoToggle(todo, isDone);
	},
	render: function () {
		var props = this.props.items,
			items = [];
		props.sort(function (a, b) {
			if (a.sort > b.sort) {
				return 1;
			}
			if (a.sort < b.sort) {
				return -1;
			}
			return 0;
		});

		props.forEach(function (item, idx) {
			var url = this.props.updateUrl + item.id;
			items.push(<ToDoItem item={item} key={idx} onToggleDone={this.handleToggle} />);
		}, this);
		return (<ul>{items}</ul>);
	}
});
module.exports = ToDoList;
var React = require('react');

var ToDoFooter = React.createClass({
	markAllAsDone: function () {
		this.props.onMarkAll();
	},
	render: function () {
		return (
			<footer>
				<span>{this.props.undone} items left</span>
				<a onClick={this.markAllAsDone}>Mark all items as done</a>
			</footer>
		);
	}
});
module.exports = ToDoFooter;
var express = require('express'),
	exphbs = require('express-handlebars'),
  	http = require('http'),
  	bodyParser = require('body-parser'),
  	routes = require('./routes');

// Create an express instance and set a port variable
var app = express();
var port = process.env.PORT || 8080;

// Set handlebars as the templating engine
app.engine('handlebars', exphbs({ defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// Index Route
app.get('/', routes.index);
app.post('/todos/add', routes.add);
app.put('/todos/update/:id', routes.update);

// Set /public as our static content dir
app.use("/", express.static(__dirname + "/public/"));

app.get('/*', routes.index);
// Fire it up (start our server)
var server = http.createServer(app).listen(port, function() {
  console.log('Listening to ' + port);
});
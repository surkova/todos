var React = require('react');
var ToDoApp = require('./components/ToDoApp.react');

var initialState = JSON.parse(document.getElementById('initial-state').innerHTML);

React.render(
	<ToDoApp todos={initialState} addUrl="/todos/add" updateUrl="/todos/update/" />, 
	document.getElementById('react-app')
);